package lesson3;

import java.util.Stack;

public class Lesson3 {
    public static void main(String[] args) {
//        MyStack<Integer> stack = new MyStack<>(3);
//        stack.push(5);
//        stack.push(15);
//        stack.push(25);
//        stack.push(25);
//        stack.push(25);
//
//        System.out.println(stack.peek());
//        System.out.println(stack.pop());
//        System.out.println(stack.pop());
//        System.out.println(stack.pop());
//        System.out.println(stack.pop());
//        System.out.println(stack.pop());
//
//        Expression expression = new Expression("(5 + 7) - [] + ({t + 4})");
//        System.out.println(expression.checkBracket());
//
//        // очередь
//        MyQueue<Integer> queue = new MyQueue<>(3);
//        System.out.println("size: " + queue.size());
//        queue.insert(10);
//        System.out.println("size: " + queue.size());
//        queue.insert(20);
//        System.out.println("size: " + queue.size());
//        queue.insert(30);
//        System.out.println("size: " + queue.size());
//        System.out.println("capacity: " + queue.capacity());
//        queue.insert(40);
//        System.out.println("size: " + queue.size());
//        System.out.println("capacity: " + queue.capacity());
//        queue.insert(50);
//        System.out.println("size: " + queue.size());
//        System.out.println("capacity: " + queue.capacity());
//        queue.insert(60);
//        System.out.println("size: " + queue.size());
//        System.out.println("capacity: " + queue.capacity());
//        queue.insert(70);
//        System.out.println("size: " + queue.size());
//        System.out.println("capacity: " + queue.capacity());
//        queue.insert(80);
//        System.out.println("size: " + queue.size());
//        System.out.println("capacity: " + queue.capacity());
//        System.out.println("queue: " + queue);
//        System.out.println(queue.remove());
//        System.out.println(queue.remove());
//        System.out.println(queue.remove());
//        System.out.println(queue.remove());
//        System.out.println(queue.remove());
//        System.out.println(queue.remove());
//        System.out.println(queue.remove());
//        System.out.println(queue.remove());
//        System.out.println("queue: " + queue);
//        System.out.println(queue.remove());


        /** Приоритетная очередь */
//        MyPriorityQueue<Integer> mpq = new MyPriorityQueue<>();
//        mpq.insert(6);
//        mpq.insert(16);
//        mpq.insert(2);
//        mpq.insert(3);
//        mpq.insert(7);
//        mpq.insert(5);
//        mpq.insert(9);
//        System.out.println(mpq);
//
//        System.out.println(mpq.remove());
//        System.out.println(mpq.remove());
//        System.out.println(mpq.remove());
//        System.out.println(mpq.remove());
//        System.out.println(mpq);

        /** Разворот строки */
        System.out.println("----- Разворот строки -----");
        MyReverseString reverseString = new MyReverseString("Hello world!");
        System.out.println("Было: " + reverseString.getString());
        System.out.println("Стало: " + reverseString.getReverseString());

        /** Приоритетная очередь. Вставка О(1), извлечение О(n) */
        System.out.println("----- Приоритетная очередь. Вставка О(1), извлечение О(n) -----");
        MyPriorityQueue<Integer> myPriorityQueue = new MyPriorityQueue<>(3);
        myPriorityQueue.insert(5);
        myPriorityQueue.insert(7);
        myPriorityQueue.insert(1);
        myPriorityQueue.insert(3);
        myPriorityQueue.insert(13);
        myPriorityQueue.insert(1);
        System.out.println(myPriorityQueue);
        System.out.println(myPriorityQueue.remove());
        System.out.println(myPriorityQueue.remove());
        System.out.println(myPriorityQueue.remove());
        System.out.println(myPriorityQueue);

        /** Двунаправленная очередь */
        System.out.println("----- Двунаправленная очередь -----");
        MyDeque<Integer> myDeque = new MyDeque<>(3);
        myDeque.backInsert(1);
        myDeque.backInsert(3);
        System.out.println("deque: " + myDeque);
        System.out.println("capacity: " + myDeque.capacity());
        System.out.println("size: " + myDeque.size());
        myDeque.backInsert(5);
        myDeque.backInsert(9);
        System.out.println("deque: " + myDeque);
        System.out.println("capacity: " + myDeque.capacity());
        System.out.println("size: " + myDeque.size());
        myDeque.frontInsert(10);
        myDeque.frontInsert(20);
        myDeque.frontInsert(30);
        myDeque.frontInsert(40);
        System.out.println("deque: " + myDeque);
        System.out.println("capacity: " + myDeque.capacity());
        System.out.println("size: " + myDeque.size());
        myDeque.frontInsert(50);
        System.out.println("deque: " + myDeque);
        System.out.println("capacity: " + myDeque.capacity());
        System.out.println("size: " + myDeque.size());
        //
        System.out.println("Элемент с конца: " + myDeque.backPeek());
        System.out.println("Элемент с начала: " + myDeque.frontPeek());
        System.out.println("Получение с конца: " + myDeque.backRemove());
        System.out.println("Получение с начала: " + myDeque.frontRemove());
        System.out.println("deque: " + myDeque);
        System.out.println("Получение с начала: " + myDeque.frontRemove());
        System.out.println("Получение с начала: " + myDeque.frontRemove());
        System.out.println("deque: " + myDeque);
    }
}
