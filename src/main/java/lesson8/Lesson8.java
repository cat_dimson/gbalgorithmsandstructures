package lesson8;

import java.util.Random;

public class Lesson8 {
    public static void main(String[] args) {
//        Random random = new Random();
//        ChainHashMap<Integer, String> map = new ChainHashMap<>(7);
//        for (int i = 0; i < 5; i++) {
//            map.put(random.nextInt(100), "a");
//        }
//        System.out.println(map);

   //     LinearProbingHashMap<Integer, String> lphm = new LinearProbingHashMap<>(97);
//        lphm.put(5, "qwe");
//        lphm.put(15, "qweerwer");
//        System.out.println(lphm.get(15));

        ChainHashMap<Integer, String> map = new ChainHashMap<>(7);
        map.put(1, "a");
        map.put(2, "б");
        map.put(3, "в");
        map.put(4, "г");
        map.put(5, "д");
        map.put(6, "е");
        map.put(7, "ж");
        map.put(8, "и");
        map.put(9, "к");
        map.put(10, "л");
        System.out.println(map);
        map.remove(4);
        System.out.println(map);
        map.remove(3);
        map.remove(10);
        System.out.println(map);
        map.remove(10);
        System.out.println(map);
        System.out.println("Размер: " + map.size());
    }
}
