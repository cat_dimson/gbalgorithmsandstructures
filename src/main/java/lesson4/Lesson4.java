package lesson4;

import java.util.ListIterator;

public class Lesson4 {
    public static void main(String[] args) {
        MyLinkedList<Integer> mll = new MyLinkedList<>();
        mll.insertFirst(4);
        mll.insertFirst(5);
        mll.insertFirst(6);
        mll.insertFirst(7);
        mll.insertFirst(8);

//        System.out.println(mll.deleteFirst());
//        System.out.println(mll.deleteFirst());
//        System.out.println(mll.deleteFirst());
//        System.out.println(mll.deleteFirst());
//        System.out.println(mll.deleteFirst());

//        System.out.println(mll.indexOf(8));
//        System.out.println(mll.indexOf(7));
//        System.out.println(mll.indexOf(10));
//        System.out.println(mll.indexOf(4));
        System.out.println(mll);
        mll.insert(0, 99);
        System.out.println(mll);
        mll.insert(3, 88);
        System.out.println(mll);
        mll.insert(7, 47);
        System.out.println(mll);
        mll.delete(99);
        System.out.println(mll);
        mll.delete(88);
        System.out.println(mll);
        mll.delete(47);
        System.out.println(mll);
        mll.delete(100);
        System.out.println(mll);
        mll.insertLast(55);
        mll.insertLast(566);

        //System.out.println(mll);

        /*System.out.println(mll.deleteFirst());
        System.out.println(mll.deleteFirst());
        System.out.println(mll);
        System.out.println(mll.deleteLast());
        System.out.println(mll.deleteLast());
        System.out.println(mll);
        System.out.println(mll.deleteLast());
        System.out.println(mll.deleteFirst());
        System.out.println(mll);
//        System.out.println(mll.deleteFirst());
        System.out.println(mll.deleteLast());
        System.out.println(mll);*/

//        System.out.println(mll);
//        mll.delete(8);
//        System.out.println(mll);
//        mll.delete(5);
//        System.out.println(mll);
//        mll.delete(566);
//        System.out.println(mll);

//        System.out.println("--- Вставка ---");
//        mll.insert(2, 123);
//        System.out.println(mll);
//        mll.insert(4, 321);
//        System.out.println(mll);
//        System.out.println(mll.deleteLast());
//        System.out.println(mll);
//        System.out.println(mll.deleteLast());
//        System.out.println(mll);
//        System.out.println(mll.deleteLast());
//        System.out.println(mll);
//        System.out.println(mll.deleteLast());
//        System.out.println(mll);
//        System.out.println(mll.deleteLast());
//        System.out.println(mll);
//        System.out.println(mll.deleteLast());
//        System.out.println(mll);

//        for (Integer el : mll) {
//            System.out.print(el + " ");
//        }

        /** MyLinkedStack */
        System.out.println("--- LinkedStack ---");
        MyLinkedStack<Integer> linkedStack = new MyLinkedStack<>();
        System.out.println(linkedStack);
        linkedStack.push(4);
        linkedStack.push(6);
        linkedStack.push(2);
        linkedStack.push(1);
        linkedStack.push(10);
        System.out.println(linkedStack);
        System.out.println(linkedStack.peek());
        System.out.println(linkedStack);
        System.out.println(linkedStack.pop());
        System.out.println(linkedStack);
        System.out.println(linkedStack.pop());
        System.out.println(linkedStack.pop());
        System.out.println(linkedStack);

        /** ListIterator */
        System.out.println("--- ListIterator ---");
        System.out.println(mll);
        ListIterator<Integer> listIterator = mll.listIterator();
        System.out.println(listIterator.hasNext());
        System.out.println("nextIndex: " + listIterator.nextIndex());
        System.out.println(listIterator.next());
        System.out.println("nextIndex: " + listIterator.nextIndex());
        System.out.println(listIterator.next());
        System.out.println(listIterator.next());
        System.out.println(listIterator.next());
        System.out.println(listIterator.next());
        System.out.println(listIterator.next());
        System.out.println(listIterator.next());
        System.out.println("nextIndex: " + listIterator.nextIndex());
        System.out.println(listIterator.next());
        System.out.println(listIterator.next());
        System.out.println("nextIndex: " + listIterator.nextIndex());
        System.out.println("---");
        System.out.println(listIterator.previous());
        System.out.println(listIterator.previous());
        System.out.println("prevIndex: " + listIterator.previousIndex());
        System.out.println(listIterator.previous());
        System.out.println(listIterator.previous());
        System.out.println(listIterator.previous());
        System.out.println("prevIndex: " + listIterator.previousIndex());
        System.out.println(listIterator.previous());
        System.out.println("prevIndex: " + listIterator.previousIndex());
        System.out.println(listIterator.previous());
        System.out.println("prevIndex: " + listIterator.previousIndex());
        System.out.println(listIterator.previous());
        System.out.println(listIterator.previous());
        System.out.println("prevIndex: " + listIterator.previousIndex());
        System.out.println(listIterator.previous());
        System.out.println(listIterator.next());
        System.out.println(listIterator.next());
        System.out.println(listIterator.next());
    }
}
