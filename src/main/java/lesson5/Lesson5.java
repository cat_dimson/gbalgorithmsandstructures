package lesson5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Lesson5 {
    final static List<Element> elements = new ArrayList<>();
    final static List<Element> bag = new ArrayList<>();

    public static void main(String[] args) {

        /** Возведение в степень через рекурсию */
        //System.out.println(recPow(15, 0));

        /** Задача о Рюкзаке */
        // создаем элементы
        final Element el1 = new Element(4, 20);
        final Element el2 = new Element(3, 18);
        final Element el3 = new Element(2, 14);
        final Element el4 = new Element(1, 10);
        final Element el5 = new Element(1, 1);
        final Element el6 = new Element(1, 2);

        // добавляем элементы в общий список и сортируем их по ценности
        elements.addAll(List.of(el1, el2, el3, el4, el5, el6));
        elements.sort(Comparator.comparingDouble(Element::valueUnitWeight).reversed());

        final int W_BAG = 9;
        final int valueBag = recBag(0, W_BAG);
        System.out.println("Начальный набор элементов: " + elements);
        System.out.println("Общая ценность собранного рюкзака: " + valueBag);
        System.out.println("Собранный рюкзак с максимальным W = " + W_BAG + ": " + bag);
    }

    // задача о Рюкзаке
    private static int recBag(int i, int w) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("Индекс не может быть отрицательным");
        }
        if (i == elements.size()) {
            return 0;
        }
        if (w - elements.get(i).getWeight() < 0) {
            return recBag(i + 1, w);
        }
        bag.add(elements.get(i));
        return elements.get(i).getValue() + recBag(i + 1, w - elements.get(i).getWeight());
    }

//        Arrays.sort(elements, Comparator.comparingDouble(Element::valueUnitWeight).reversed());
//
//        System.out.println(Arrays.toString(elements));
//
//        final int W = 7;
//
//        int weightSoFar = 0;
//        //
//        double valueSoFar = 0;
//        // индекс текущего предмета
//        int i = 0;
//
//        while (i < elements.length && weightSoFar != W) {
//            if (weightSoFar + elements[i].getWeight() < W) {
//                // берем обхъект целиком
//                valueSoFar += elements[i].getValue();
//                weightSoFar += elements[i].getWeight();
//            } else {
//                valueSoFar += ((W - weightSoFar) / (double) elements[i].getWeight()) * elements[i].getValue();
//                weightSoFar = W;
//            }
//
//            i++;
//        }
//
//        System.out.println("Ценность лучшего набора: " + valueSoFar);


//    private static long fibo(int n) {
//        long a = 1;
//        long b = 1;
//        for (int i = 3; i < n; i++) {
//            b = a + b;
//            a = b - a;
//        }
//        return b;
//    }
//
//    private static int multiply(int a, int b) {
//        int p = 0;
//        for (int i = 0; i < b; i++) {
//            p += a;
//        }
//        return p;
//    }
//
//    private static int recMultiply(int a, int b) {
//        if (b == 0) {
//            return 0;
//        }
//        return recMultiply(a, b - 1) + a;
//    }
//
//    private static int triangleNum(int n) {
//        int sum = 0;
//        for (int i = 1; i <= n; i++) {
//            sum += i;
//        }
//        return sum;
//    }
//
//    private static int recTriangleNum(int n) {
//        if (n == 1) {
//            return 1;
//        }
//        return recTriangleNum(n - 1) + n;
//    }
//
//    private static long recfibo(int n) {
//        if (n < 3) {
//            return 1;
//        }
//        return recfibo(n - 1) + recfibo(n - 2);
//    }
//
//    private static int fact(int n) {
//        int f = 1;
//        for (int i = 2; i <= n; i++) {
//            f *= i;
//        }
//        return f;
//    }
//
//    private static int factorial(int n) {
//        if (n == 1) {
//            //throw new RuntimeException("эацуэацуэп");
//            return 1;
//        } else {
//            return factorial(n - 1) * n;
//        }
//    }


    // возведение в степень с помощью рекурсии
    private static int recPow(int num, int power) {
        if (power <= 0 && num == 0) {
            throw new ArithmeticException();
        }
        if (power == 0) {
            return 1;
        } else if (power < 0) {
            return 1 / num * recPow(num, power + 1);
        } else {
            return num * recPow(num, power - 1);
        }
    }
}
