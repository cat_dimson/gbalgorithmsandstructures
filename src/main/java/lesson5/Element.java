package lesson5;

public class Element {
    private int weight;
    private int value;

    public Element(int weight, int value) {
        this.weight = weight;
        this.value = value;
    }

    public double valueUnitWeight() {
        return value / (double) weight;
    }

    public int getWeight() {
        return weight;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "{ w:" + weight + ", v:" + value + " }";
    }
}
