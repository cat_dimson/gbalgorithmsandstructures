package lesson7;

public class Lesson7 {
    public static void main(String[] args) {
//        Graph graph = new Graph(5);

//        graph.addEdge(0, 2);
//        graph.addEdge(1, 3);
//        graph.addEdge(3, 2);
//        graph.addEdge(4, 2);
//        graph.addEdge(3, 4);
//        graph.addEdge(1,2);

        //System.out.println(graph.getEdgeCount());
        //System.out.println(graph.getAdjList(2));

//        DepthFirstPath dfp = new DepthFirstPath(graph, 1);
//        System.out.println(dfp.hasPathTo(0));
//        System.out.println(dfp.pathTo(0));

//        BreadthFirstPaths bfp = new BreadthFirstPaths(graph, 1);
//        System.out.println(bfp.hasPathTo(0));
//        System.out.println(bfp.pathTo(0));


        /** Домашняя работа */
        Graph graph = new Graph(10);

        graph.addEdge(1,3);
        graph.addEdge(3,9);
        graph.addEdge(9,4);
        graph.addEdge(9,2);
        graph.addEdge(4,2);
        graph.addEdge(4,5);
        graph.addEdge(2,5);
        graph.addEdge(4,7);
        graph.addEdge(5,7);
        graph.addEdge(0,2);
        graph.addEdge(0,5);
        graph.addEdge(0,6);
        graph.addEdge(8,6);

        BreadthFirstPaths bfp = new BreadthFirstPaths(graph, 1);
        System.out.println(bfp.hasPathTo(8));
        System.out.println(bfp.pathTo(8));
        System.out.println("Длина пути: " + bfp.lengthTo(8));
    }
}
