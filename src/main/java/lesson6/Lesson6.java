package lesson6;

import java.util.Random;

public class Lesson6 {
    public static void main(String[] args) {
//        MyTreeMap<Integer, String> map = new MyTreeMap<>();
//
//        map.put(4, "four");
//        map.put(2, "two");
//        map.put(1, "one");
//        map.put(6, "шесть");
//        map.put(3, "three");
//
//        System.out.println(map);
//        map.delete(2);
//        System.out.println(map);
//        System.out.println("height: " + map.height());
//        map.put(8, "восемь");
//        System.out.println("height: " + map.height());
//        map.put(5, "пять");
//        System.out.println("height: " + map.height());
//        map.put(7, "семь");
//        System.out.println("height: " + map.height());
//        System.out.println("isBalanced: " + map.isBalanced());
//        map.put(9, "девять");
//        System.out.println("height: " + map.height());
//        System.out.println("isBalanced: " + map.isBalanced());
//        map.put(10, "десять");
//        System.out.println("height: " + map.height());
//        System.out.println("isBalanced: " + map.isBalanced());
//        System.out.println("Size: " + map.size());

        /*System.out.println(map);
        System.out.println(map.isBalanced());
        System.out.println("size: " + map.size());
        System.out.println("height: " + map.height());*/

        int MAX_COUNT = 100_000;
        int notBalancedCount = 0;
        int balancedCount = 0;

        for (int i = 0; i < MAX_COUNT; i++) {
            MyTreeMap<Integer, Integer> map = generateMyTreeMap();
            if (map.isBalanced()) {
                balancedCount++;
            } else {
                notBalancedCount++;
            }
        }

        printStatistic(notBalancedCount, balancedCount, MAX_COUNT);
    }

    public static MyTreeMap<Integer, Integer> generateMyTreeMap() {
        MyTreeMap<Integer, Integer> genTreeMap = new MyTreeMap<>();
        int min = -100;
        int max = 101;

        while (true) {
            int num = (int) (Math.random() * (max - min) + min);
            genTreeMap.put(num, num);
            if(genTreeMap.height() > 6) {
                genTreeMap.delete(num);
                break;
            }
        }

        return genTreeMap;
    }

    public static void printStatistic(int notBalancedCount, int balancedCount, int count) {
        float notBalancedPercent = (float) notBalancedCount * 100 / (float) count;
        float balancedPercent = 100 - notBalancedPercent;
        System.out.println("Не сбалансированных: " + notBalancedPercent + "%");
        System.out.println("Сбалансированных: " + balancedPercent + "%");
    }
}
