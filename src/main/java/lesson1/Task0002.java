package lesson1;

import java.io.PrintWriter;
import java.util.Scanner;

//ID0002
public class Task0002 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int a = in.nextInt();

        out.println(getSum(a));
        out.flush();
    }

    public static int getSum(int a) {
        int sum = 0;
        for (int i = 1; i <= Math.abs(a); i++) {
            sum += i;
        }
        if (a < 0) {
            sum = -1 * sum + 1;
        }
        if (a == 0) {
            sum = 1;
        }
        return sum;
    }
}
