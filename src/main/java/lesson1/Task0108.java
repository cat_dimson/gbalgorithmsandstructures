package lesson1;
import java.io.*;
import java.util.*;

//ID0108
public class Task0108 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int a = in.nextInt();
        out.println(a);

        out.flush();
    }
}
