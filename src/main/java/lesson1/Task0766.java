package lesson1;

import java.io.PrintWriter;
import java.util.Scanner;

//ID0002
public class Task0766 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int n = in.nextInt();
        int m = in.nextInt();
        int k = in.nextInt();

        out.println(getResult(n,m,k));
        out.flush();
    }

    private static String getResult(int n, int m, int k) {
        return n * m >= k ? "YES" : "NO";
    }
}
