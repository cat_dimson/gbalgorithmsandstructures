package lesson1;

import java.io.PrintWriter;
import java.util.Scanner;

//ID0903
public class Task0903 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int a = in.nextInt();
        out.println(++a);

        out.flush();
    }
}
