package lesson2;

public class MySortedArrayList<T extends Comparable<T>> extends MyArrayList<T> {

    @Override
    public void add(T item) {
        int i = 0;
        while (i < size() && item.compareTo(get(i)) >= 0) {
            i++;
        }

        super.insert(i, item);
    }

    @Override
    public void insert(int index, T item) {
        add(item);
    }

    public int binaryFind(T item) {
        int low = 0;
        int high = size() - 1;
        int mid;

        while (low <= high) {
            mid = low + (high - low) / 2;
            if (item.compareTo(get(mid)) < 0) {
                high = mid - 1;
            } else if (item.compareTo(get(mid)) > 0){
                low = mid + 1;
            } else {
                return mid;
            }
        }
        return -1;
    }

    public int recBinaryFind(T item) {
        return binaryFind(item, 0, size() - 1);
    }

    private int binaryFind(T item, int low, int high) {
        int mid = low + (high - low) / 2;
        if (item.compareTo(get(mid)) == 0) {
            return mid;
        }
        if (low == high) {
            return -1;
        } else if (item.compareTo(get(mid)) < 0){ // item < серединного элемента
            return binaryFind(item, low, mid);
        } else {
            return binaryFind(item, mid + 1, high);
        }
    }
}
